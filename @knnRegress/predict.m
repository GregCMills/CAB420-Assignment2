% Test function: predict on Xtest
function Yte = predict(obj,Xte)
    % get size of training, test data
    [Ntr,Mtr] = size(obj.Xtrain);
    [Nte,Mte] = size(Xte);
    
    % make Ytest the same data type as Ytrain
    Yte = repmat(obj.Ytrain(1), [Nte,1]);  
    % can't have more than Ntrain neighbors
    K = min(obj.K, Ntr);
    
    % For each test example:
    for i=1:Nte,
        % compute sum of squared differences
        dist = sum( bsxfun( @minus, obj.Xtrain, Xte(i,:) ).^2 , 2);
        
        % find nearest neighbors over Xtrain (dimension 2)
        [tmp,idx] = sort(dist);              
        cMax=1; NcMax=0; 
        
        % Get the average value of the k nearest neighbours.
        Yte(i)= mean(obj.Ytrain(idx(1:K)));    
    end;
end
