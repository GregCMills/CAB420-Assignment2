clear ; close all; clc

%load shuffle and assign data to objects for use
iris = load('data\iris.txt');
X = iris(:,1:2); Y=iris(:,end);
UntouchedX = X;
[X, Y] = shuffleData(X,Y);
X = rescale(X);
XA = X(Y<2,:); YA=Y(Y<2);
XB = X(Y>0,:); YB=Y(Y>0);

%a
%scatter of A data
figure('name', 'Iris type by Sepal Length and Width','NumberTitle','off')
scatter(XA(YA==0, 1), XA(YA==0, 2), 30, [0 0 1]);
hold on
scatter(XA(YA==1, 1), XA(YA==1, 2), 30, [1 0 0]);
title('Data A Iris type by Sepal Length and Width')
legend('Iris-setosa', 'Iris-versicolor')
xlabel('Sepal Length (cm)')
ylabel('Sepal Width (cm)')
hold off
%scatter of B data
figure('name', 'Iris type by Sepal Length and Width','NumberTitle','off')
scatter(XB(YB==2, 1), XB(YB==2, 2),30,[0 0 1]);
hold on
scatter(XB(YB==1, 1), XB(YB==1, 2),30,[1 0 0]);
title('Data B Iris type by Sepal Length and Width')
legend( 'Iris-virginica','Iris-versicolor','location','northwest')
xlabel('Sepal Length (cm)')
ylabel('Sepal Width (cm)')
hold off

%b
%creation of learner object with A data
learnerA = logisticClassify2();
learnerA=setClasses(learnerA, unique(YA));
wts = [0.5,1,-0.25];
learnerA=setWeights(learnerA, wts);
%creation of learner object with B data
learnerB = logisticClassify2();
learnerB=setClasses(learnerB, unique(YB));
wts = [0.5,1,-0.25];
learnerB=setWeights(learnerB, wts); 

%plot of A data with decision boundary
figure('name', 'Iris type by Sepal Length and Width','NumberTitle','off')
plot2DLinear(learnerA, XA, YA);
title('Data A Iris type by Sepal Length and Width Data A with decision boundary')
legend('Iris-setosa', 'Iris-versicolor','Decision Boundary')
xlabel('Sepal Length (cm)')
ylabel('Sepal Width (cm)')
%plot of B data with decision boundary
figure('name', 'Iris type by Sepal Length and Width','NumberTitle','off')
plot2DLinear(learnerB, XB, YB);
title('Data B Iris type by Sepal Length and Width with decision boundary')
legend('Iris-virginica', 'Iris-versicolor','Decision Boundary')
xlabel('Sepal Length (cm)')
ylabel('Sepal Width (cm)')

%c
%calculation of error according to decision boundary
yte = predict(learnerA,XA);
error = sum(yte ~= YA) / length(YA)
figure('name', 'Iris type by Sepal Length and Width','NumberTitle','off')
plotClassify2D(learnerA,XA,YA) %plot using plotClassify2D to confirm correctness
title('Data A Iris type by Sepal Length and Width with decision boundary')
legend('Iris-setosa', 'Iris-versicolor')
xlabel('Sepal Length (cm)')
ylabel('Sepal Width (cm)')

%d
%e
%f
%train(learnerA, XA, YA,'stoptol',0.000001,'stepsize',4);
train(learnerB, XB, YB,'stoptol',0.000001);