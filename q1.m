% Clear Existing data and load new data
clear ; close all; clc;

mTrain=load('data/mcycleTrain.txt');
ytr=mTrain(:,1);
xtr=mTrain(:,2);

mTest=load('data/mcycleTest.txt');
testytr=mTest(:,1);
testxtr=mTest(:,2);
% ************************************

% Q(a) - Plot Scatter of Data ********
figure('Name', 'Scatter Plot of Motorcycle Data');
scatter(xtr, ytr, 'bo');
title('Scatter Plot of Motorcycle Data');
ylabel('Accelaration');
xlabel('Time in Seconds');
% ************************************

% Q(b) Linear Plot *********************
figure('Name', 'Linear Predictor');
scatter(xtr, ytr, 'bo');
title('Linear Predictor of Motorcycle Data');
ylabel('Accelaration');
xlabel('Time in Seconds');

Xtr = [ones(size(xtr,1),1), xtr];
testXtr = [ones(size(testxtr,1),1), testxtr];

learner = linearReg(Xtr, ytr);
yhat = predict(learner, Xtr);

xline = [0:.01:2]'; % transpose: make a column vector, like training x 
yline = predict(learner, polyx(xline,1) ); % assuming quadratic features
hold on

plot(xline, yline, 'r');
legend('Acceleration / Time', 'Linear Predictor');
% ***************************************

% Q(d) MSE on training data (Linear) *************
error = mse(learner, Xtr, ytr);
fprintf('Linear Predictor, Training Data MSE: %f \n', error)
% ************************************************

% Q(e) MSE on testing data (Linear) *************
error = mse(learner, testXtr, testytr);
fprintf('Linear Predictor, Testing Data MSE: %f \n', error)
% ***********************************************

% Q(c) Plot 5 degree predictor ********************
figure('Name', '5th degree');
plot(xtr, ytr, 'bo');
title('5th Degree Polynomial Predictor of Motorcycle Data');
ylabel('Accelaration');
xlabel('Time in Seconds');

Xtr = polyx(xtr, 5); % Expand the training data to 5 degrees
testXtr = polyx(testxtr, 5); % Expand t

learner = linearReg(Xtr, ytr);
yhat = predict(learner, Xtr);

xline = [0:.01:2]'; % transpose: make a column vector, like training x 
yline = predict(learner, polyx(xline,5) ); % assuming quadratic features
hold on
plot(xline, yline, 'r');
legend('Acceleration / Time','5th Degree Polynomial Predictor');
% ****************************************************

% Q(d) MSE on training data (5 degree) *************
error = mse(learner, Xtr, ytr);
fprintf('5 Degree Polynomial Predictor, Training Data MSE: %f \n', error);
% **************************************************

% Q(e) MSE on testing data (5 degree) *************
error = mse(learner, testXtr, testytr);
fprintf('5 Degree Polynomial Predictor, Testing Data MSE: %f \n', error)
% *************************************************





