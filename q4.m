clear ; close all; clc

%load and format data
iris = load('data\iris.txt');
pi = randperm(size(iris,1));
Y=iris(pi,5); X=iris(pi, 1:2);

%a
%plot each class seperately
figure('name', 'Iris type by Sepal Length and Width','NumberTitle','off')
scatter(X(Y==0, 1), X(Y==0, 2), 30, [0 0 1]);
hold on
scatter(X(Y==1, 1), X(Y==1, 2), 30, [0 1 0]);
hold on
scatter(X(Y==2, 1), X(Y==2, 2), 30, [1 0 0]);
title('Iris type by Sepal Length and Width')
legend('Iris-setosa', 'Iris-versicolor', 'Iris-virginica')
xlabel('Sepal Length (cm)')
ylabel('Sepal Width (cm)')
hold off

%b
%c
%Create each learner for value of k then plot
for k = [1,3,10,30]
   learner = knnClassify(k, X, Y);
   class2DPlot(learner, X, Y);
   plotTitle = strcat(int2str(k),' Nearest Neighbour Classify');
   title(plotTitle)
   legend('Iris-setosa', 'Iris-versicolor', 'Iris-virginica')
   xlabel('Sepal Length (cm)')
   ylabel('Sepal Width (cm)')  
   
end

%d
% first split data into Xtrain,Ytrain and Xvalid,Yvalid
trainRatio = 0.8;
trainNum = round(trainRatio * size(X,1));
xTrain = X(1:trainNum, :);
yTrain = Y(1:trainNum);
xTest = X((trainNum + 1):end,:);
yTest = Y((trainNum + 1):end);

% set k values, create learners and train, record errors
kvalues=[1,2,5,10,50,100,200]; 
err = zeros(1,7);
for i=1:length(kvalues) 
    learner = knnClassify(kvalues(i), xTrain, yTrain); % train model on X/Ytrain 
    Yhat = predict(learner, xTest); % predict results on X/Yvalid 
    numWrongPred = sum(Yhat ~= yTest);
    err(i) = numWrongPred; % count what fraction of predictions are wrong 
end
% calculate incorrect prediction rate and graph and graph vs K
figure('name', 'Number of Incorrect Predictions vs K value','NumberTitle','off')
plot(kvalues, err/30);
title('Number of Incorrect Predictions vs K value')
ylim([0,1])
xlabel('K Value');
ylabel('Incorrect Prediction Rate');

