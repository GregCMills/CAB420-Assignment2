% Clear Existing data and load new data*****
clear ; close all; clc

mTrain=load('data/mcycleTrain.txt');
ytr=mTrain(:,1); xtr=mTrain(:,2);

mTest=load('data/mcycleTest.txt');
testytr=mTest(:,1); testxtr=mTest(:,2);

% *******************************************
% Q3(a) Compute/Plot MSE vs K for first 20 training values 
titleStr = 'MSE/k for first 20 Training Values.';
figure('Name', titleStr);
legendArr = {'MSE / k trained on first 20 values', 'Minimum MSE'};

plotMSEkNearestReg(1:100, xtr(1:20), ytr, testxtr, testytr, legendArr);

title(titleStr);
% *******************************************

% Q3(b) Compute/Plot MSE vs K for all training values 
titleStr = 'MSE/k for all Training Values.';
figure('Name', titleStr);

plotMSEkNearestReg(1:100, xtr(1:20), ytr, testxtr, testytr, {});

legendArr = {'MSE / k trained on first 20 values values',...
    'Minimum MSE (first 20 training values)', 'MSE / k trained on all values',...
    'Minimum MSE (all training values)'};
plotMSEkNearestReg(1:100, xtr, ytr, testxtr, testytr, legendArr);

title(titleStr);
% *******************************************

% Q3(c) 4-fold cross-validation *************
% Create a new figure with the previous two error plots
titleStr = 'MSE/k 4-fold Cross-Validation.';
figure('Name', titleStr);
plotMSEkNearestReg(1:100, xtr(1:20), ytr, testxtr, testytr, {});
plotMSEkNearestReg(1:100, xtr, ytr, testxtr, testytr, {});

% Get a range of k values
kValues = 1:100;

% Get 4 MSE values for each k value, 
% one for each quarter of data being seperated for test data.
mseMat = zeros(100,4);
for k = kValues 
    for xval=1:4, 
        iTest = ((xval-1)*20 + 1) : ((xval-1)*20 + 20); % choose 20 indices for testing 
        iTrain = setdiff(1:80, iTest); % rest for training 
        learner = knnRegress(k, xtr(iTrain), ytr); % train on X(iTrain,:) 
        mseMat(k,xval) = mse(learner, xtr(iTest), ytr(iTest)); % test on X(iTest,:) 
    end;
end; 

% Average the error values for each value of k.
averageError = mean(mseMat');

% Plot on a log scale graph.
loglog(1:100, averageError);

% Calculate the minimum error points
% (Taken from https://goo.gl/KmjgKf)
indexmin = find(min(averageError) == averageError); 
xmin = kValues(indexmin);
ymin = averageError(indexmin);
strmin = sprintf('Min MSE = %.2f \n @ k = %d',...
    ymin, xmin);

% Plot the minimum point.
offsetPercentage = 0.3; % Lower the text a little
text(xmin,(ymin - (offsetPercentage*ymin)), strmin, ...
    'HorizontalAlignment','center');
hold on
scatter(xmin,ymin,'filled','g');

% Label the plot with appropriate labels.
title(titleStr);
legendArr = {'MSE / k trained on first 20 values values',...
    'Minimum MSE (first 20 training values)',...
    'MSE / k trained on all values',...
    'Minimum MSE (all training values)',...
    'MSE / k using 4-fold Cross-Validation'...
    'Minimum MSE (4-fold Cross-Validation)'};
legend(legendArr);
xlabel('Log log K');
ylabel('log logMSE');
% *************************************





