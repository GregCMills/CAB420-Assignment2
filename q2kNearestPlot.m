function q2kNearestPlot( kValues, xtr, ytr, yLabel, xLabel,...
    figName, legendString )
%Q2PLOT Plots predicted functions for kValues, training on 
% xtr training values and ytr class values.

% Plot a scatter graph for data
figure('Name', figName);
scatter(xtr, ytr, 'm');
title(figName);
ylabel(yLabel, 'Interpreter', 'none');
xlabel(xLabel, 'Interpreter', 'none');
hold on;
% **************************************

xline = [min(xtr):.01:max(xtr)]'; % transpose: make a column vector, like xtr

% Plot the k-Nearest neighbour predictor line for kvalues ********
for k = kValues
    % Train the model
    learner = knnRegress(k, xtr, ytr);
    
    % Create and plot the predictor line
    yline = predict(learner, xline ); % assuming quadratic features
    plot(xline, yline);
    
    % Add the lines to the legend string array.
    lineLegend = sprintf('%d nearest neighbours predictor', k);
    legendString = [legendString, lineLegend];
end
% *****************************************************************

legend(legendString, 'Interpreter', 'none'); % Add the legend.
hold off;
end

