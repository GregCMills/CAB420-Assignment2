% Clear Existing data and load new data
clear ; close all; clc

mTrain=load('data/mcycleTrain.txt');
ytr=mTrain(:,1);
xtr=mTrain(:,2);
% ************************************
% Q2(b) Plot the k nearest neighbour predictor for various k values,
yLabel = 'Acceleration';
xLabel = 'Time in Seconds';

figName = sprintf('K Nearest Neighbours');
legendString = {'Acceleration / Time'};
q2kNearestPlot([1,2], xtr, ytr, yLabel, xLabel, figName, legendString);
q2kNearestPlot([3,5], xtr, ytr, yLabel, xLabel, figName, legendString);
q2kNearestPlot([10,50], xtr, ytr, yLabel, xLabel, figName, legendString);
q2kNearestPlot(80, xtr, ytr, yLabel, xLabel, figName, legendString);
% ************************************

% Q2(c) ******************************
x = [-10:.5:10]';
k = 4;

% Positive power function
y = x.^2;
q2kNearestPlot([k], x, y, 'y = x^2', 'x',...
    'Positive Power Function', {'y = x^2'});
% Negative Power Function
y = x.^(-2);
q2kNearestPlot([k], x, y, 'y = x^(-2)', 'x',...
    'Negative Power Function', {'y = x^(-2)'});

% Polynomial Function
y = ((x-1).^2 .* (x + 1));
q2kNearestPlot(k, x, y, 'y = (x - 2)^2 * (x + 1)', 'x',...
    'Polynomial Function', {'y = (x - 2)^2 * (x + 1)'});

% Rational Function
y = ((x.^2 + 1)./x);
q2kNearestPlot(k, x, y, 'y = (x^2 + 1)/x', 'x',...
    'Rational Function', {'y = (x^2 + 1)/x'});

% circle function
y1 = sqrt(100 - (x).^2);
y2 = -1* sqrt(100 - (x).^2);
q2kNearestPlot(k, cat(1, x, x),...
    cat(1, y1, y2), 'y = +/- sqrt(25 - x)^2', 'x',...
    'Circle Function',{'y = +/- sqrt(25 - x)^2'} );

% parametric function
xt = x.^2;
y = 2*x;
titleStr = sprintf('Parametric Function (t = %d to %d)', min(x), max(x));
q2kNearestPlot(k, xt, y, 'y = 2t', 'x = t^2',...
    titleStr, {'y = 2t, x = t^2'});
% ************************************