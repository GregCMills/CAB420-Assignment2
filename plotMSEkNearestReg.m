function plotMSEkNearestReg( kValues, xtr, ytr, testxtr, testytr, legendArr)
%plotMSEkNearestReg Plots the MSE for a k-nearest regression model.

% Calculate the MSE against given test data for each k value.
error = zeros(length(kValues),1);
for k = kValues % For various values of k
    learner = knnRegress(k, xtr, ytr);
    error(k) = mse(learner, testxtr, testytr); % Calculate the error
end

% Calculate the minimum error points
% (Taken from https://goo.gl/KmjgKf)
indexmin = find(min(error) == error); 
xmin = kValues(indexmin);
ymin = error(indexmin);
strmin = sprintf('Min MSE = %.2f \n @ k = %d',...
    ymin, xmin);

% Plot the error / kvalues on a loglog scale
loglog(kValues, error);
xlabel('k values');
ylabel('Mean Squared Error for k');

% Print the minimum point.
offsetPercentage = 0.3; % Lower the text a little
text(xmin,(ymin - (offsetPercentage*ymin)), strmin, ...
    'HorizontalAlignment','center');
hold on
scatter(xmin,ymin,'filled');

% Add legend
legend(legendArr);
% *******************************************

end